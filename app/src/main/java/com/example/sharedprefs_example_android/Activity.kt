package com.example.sharedprefs_example_android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sharedprefs_example_android.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.text = "Click Count: ${DefaultSharedPrefs.clickCount}"
        binding.button.setOnClickListener {
            handleButtonClick()
        }
    }

    private fun handleButtonClick() {
        binding.button.text = "Click Count: ${++DefaultSharedPrefs.clickCount}"
    }
}