package com.example.sharedprefs_example_android

import android.content.Context
import android.content.SharedPreferences

private const val PREF_NAME = "defaultSharedPrefs"

object DefaultSharedPrefs : SharedPreferences by sharedApplicationContext.getSharedPreferences(
    PREF_NAME, Context.MODE_PRIVATE
)