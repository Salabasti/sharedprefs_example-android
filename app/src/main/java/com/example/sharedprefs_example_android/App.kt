package com.example.sharedprefs_example_android

import android.app.Application
import android.content.Context
import java.lang.IllegalStateException

val sharedApplicationContext: Context get() = sharedApplicationContextBackingProperty
    ?: throw IllegalStateException(
        "Application context not initialized yet."
    )
private var sharedApplicationContextBackingProperty: Context? = null

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        sharedApplicationContextBackingProperty = applicationContext
    }
}